/*var http = require('http');

http.createServer( function(request, response) {
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end('hello world 11111111');

}).listen(8085);

console.log('Server running at http://127.0.0.1:8085/');*/

/*var events = require('events');
// Create an eventEmitter object
var eventsEmitter = new events.EventEmitter();

var eventBinding = function binding(){
	console.log("bonding succesfully");

	eventsEmitter.emit('testing2');
}

eventsEmitter.on('testing', eventBinding);



eventsEmitter.on('testing2', function(){
	console.log("testing2");
});

eventsEmitter.emit('testing');*/

var events = require('events');
var eventEmitter = new events.EventEmitter();

// listener #1
var listner1 = function listner1() {
   console.log('listner1 executed.');
}

// listener #2
var listner2 = function listner2() {
  console.log('listner2 executed.');
}

// Bind the connection event with the listner1 function
eventEmitter.addListener('connection', listner1);

// Bind the connection event with the listner2 function
eventEmitter.on('connection', listner2);

var eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log(eventListeners + " Listner(s) listening to connection event");

// Fire the connection event 
eventEmitter.emit('connection');

// Remove the binding of listner1 function
eventEmitter.removeListener('connection', listner1);
console.log("Listner1 will not listen now.");

// Fire the connection event 
eventEmitter.emit('connection');

eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log(eventListeners + " Listner(s) listening to connection event");

console.log("Program Ended.");
console.log( __filename );
var http = require('http');
var fs = require('fs');
var url = require('url');

http.createServer(function(request, response){
	var pathName = url.parse(request.url).pathname;

	console.log("request for th e file =>"+pathName);

	fs.readFile(pathName.substr(1), function(err, data){
		if(err){
			console.log("page not found",err);
			response.writeHead(404,{'Content-Type':'text/html'});
		}else{
			response.writeHead(200,{'Content-Type':'text/html'});
			console.log("page found");
			response.write(data.toString());
		}
		response.end();
	})
	
}).listen(8086);
// Console will print the message
console.log('Server running at http://127.0.0.1:8086/');